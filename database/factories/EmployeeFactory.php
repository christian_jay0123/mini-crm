<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Model\Employee::class, function (Faker $faker) {
    return [
        'firstname'=>$faker->firstName,
        'lastname'=>$faker->lastName,
        'email'=>$faker->unique()->safeEmail,
        'password'=> Hash::make('password'),
        'company_id' => 1,
    ];
});
