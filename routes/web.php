<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




//set the language of the app
Route::group(['prefix'=>'{language}'],function(){
    
    

    Route::get('', function () {
        return view('welcome');
    });

    //Login form in employee page
    Route::get('/login','Auth\EmployeeController@showLoginForm')->name('employee.loginform');
    
    //login function employee page
    Route::post('/login','Auth\EmployeeController@login')->name('employee.login');

    //Index page for employee
    Route::resource('/index','Employee\EmployeeController',array("as"=>"employee"));
    
    //redirect to admin page
    Route::group(['prefix'=> 'admin'],function()
    {
        //build in auth
        Auth::routes(['register'=> false,'reset'=>false,'verify'=>false]);

        //Index page for administrator
        Route::resource('index','Admin\AdminController');
        Route::resource('company','Admin\Company\CompanyController');
        Route::resource('company/{company_id}/employee','Admin\Employee\EmployeeController');
    });
});

