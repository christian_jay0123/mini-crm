@extends('admin.dashboard')


@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
@endsection

@section('nav')
@include('admin.includes.nav')

@stop

@section('content')
<div class="row">
    <div class="col-12">
        <div class="icon float-right">
        </div>
    </div>
</div>

<form method="POST" action="/{{$data['language']}}/admin/company/{{$data['company']->id}}" enctype="multipart/form-data">
    @method('PUT')
<div class="form-group">
    <label for="exampleInputEmail1">Company Email address</label>
    <input name="email" type="email" class="form-control" value="{{$data['company']->email}}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Company email">
  </div>
  <div class="form-group">
    <label for="exampleInputFirstName">Company Name</label>
    <small style="color:red;">
    @error('name')
        {{$message}}
        @enderror
    </small>
    <input name="name"  value="{{$data['company']->name}}" type="text" class="form-control" id="exampleInputFirstName" aria-describedby="emailHelp" placeholder="Enter Company name">
  </div>
  <div class="form-group">
    <label for="exampleInputPhoneNumber1">Website</label>
    <input name="website" type="text" value="{{$data['company']->website}} "class="form-control" id="exampleInputPhoneNumber1"  placeholder="Enter Website">
  </div>
  <div class="form-group">
    <label for="exampleLogo1">Logo</label>
    <small style="color:red;">@error('logo')
        {{$message}}
        @enderror
    </small>
    <input name="logo" type="file" class="form-control" id="exampleLogo1"  placeholder="Enter New Password">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
  @csrf
</form>

@stop

@section('js')

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>

@endsection