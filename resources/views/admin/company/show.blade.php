@extends('admin.dashboard')


@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
@endsection

@section('nav')
@include('admin.includes.nav')

@stop

@section('content')
@include('admin.company.stat-box')
<div class="row">
    
    <div class="col-12">
        <div class="icon float-left">
            <img src={{asset("/images/company/logo/$company->logo")}} width="100px" height="100px" alt="logo.png">
        </div>
        <div class="icon float-right">
            <a href="{{$company->id}}/employee/create" class="ion ion-person-add" style="zoom:5.0;color:green;"></a>
        </div>
    </div>
</div>
<br>
<table id="example" class="table table-striped table-bordered" style="width:100%">
    <thead>
        <tr>
            <th>ID Number</th>
            <th>Name</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Date Hired</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($company->employees as $key=> $employee)
        <tr>
            <td>{{$employee->id}}</td>
            <td>{{$employee->lastname.', '.$employee->firstname}}</td>
            <td>{{$employee->phone}}</td>
            <td>{{$employee->email}}</td>
            <td>{{$employee->created_at}}</td>
            <td>
                <form method="POST" action="{{$company->id}}/employee/{{$employee->id}}">
                    <a href="{{$company->id}}/employee/{{$employee->id}}/edit" class="btn btn-primary">Update Profile</a>
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-danger">Delete</a>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>


@stop

@section('js')

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready( function () {
        $('#example').DataTable();
    } );
</script>

@endsection