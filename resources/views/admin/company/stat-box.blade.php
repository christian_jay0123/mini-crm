<div class="row">
    <!-- ./col -->
    <div class="col-lg-12 col-12">
      <!-- small box -->
      <div class="small-box bg-warning">
        <div class="inner">
          <h3>{{$company->employees->count()}}</h3>

          <p>{{__('msg.Total-Registered-Employee') }}</p>
        </div>
        <i class="small-box-footer"><br></i>
      </div>
    </div>
    <!-- ./col -->
  </div>