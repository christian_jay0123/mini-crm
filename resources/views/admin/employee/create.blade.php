@extends('admin.dashboard')


@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
@endsection

@section('nav')
@include('admin.includes.nav')

@stop

@section('content')
<div class="row">
  <div class="col-12">
      <div class="icon float-left">
        <a href="/{{$data['language']}}/admin/company/{{$data['company_id']}}"><i class="fas far fa-arrow-left text-danger"></i></a>
      </div>
  </div>
</div>

<form method="POST" action="/{{$data['language']}}/admin/company/{{$data['company_id']}}/employee">
<div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
  </div>
  <div class="form-group">
    <label for="exampleInputFirstName">firstname</label>
    <input name="firstname" type="text" class="form-control" id="exampleInputFirstName" aria-describedby="emailHelp" placeholder="Enter Firstname">
  </div>
  <div class="form-group">
    <label for="exampleInputLastName1">Last name</label>
    <input name="lastname" type="text" class="form-control" id="exampleInputLastName1"  placeholder="Enter Lastname">
  </div>
  <div class="form-group">
    <label for="exampleInputPhoneNumber1">Phone Number</label>
    <input name="phone" type="text" class="form-control" id="exampleInputPhoneNumber1"  placeholder="Enter Phone Number">
  </div>
  <div class="form-group">
    <label for="exampleInputNewPassword1">Password</label>
    <input name="password" type="password" class="form-control" id="exampleInputNewPassword1"  placeholder="Enter New Password">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
  @csrf
</form>

@stop

@section('js')

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>

@endsection