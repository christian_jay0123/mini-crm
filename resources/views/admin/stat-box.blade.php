<div class="row">
    <!-- ./col -->
    <div class="col-lg-6 col-6">
      <!-- small box -->
      <div class="small-box bg-success">
        <div class="inner">
          <h3>{{$data['companies']->count()}}</h3>

          <p>{{ __('msg.Registered-Company') }}</p>
        </div>
        <div class="icon">
          <i class="ion ion-stats-bars"></i>
        </div>
        <i  class="small-box-footer"> <br></i>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-6 col-6">
      <!-- small box -->
      <div class="small-box bg-warning">
        <div class="inner">
          <h3>{{$data['employees']}}</h3>

          <p>{{__('msg.Total-Registered-Employee') }}</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        <i class="small-box-footer"><br></i>
      </div>
    </div>
    <!-- ./col -->
  </div>