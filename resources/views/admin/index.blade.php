@extends('admin.dashboard')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
@endsection

@section('nav')
@include('admin.includes.nav')
@endsection

@section('content')
@include('admin.stat-box')
<div class="row">
    <div class="col-12">
        <div class="icon float-right">
            <a href="/{{$data['language']}}/admin/company/create" class="ion ion-person-add" style="zoom:5.0;color:green;"></a>
        </div>
    </div>
</div>
<table id="example" class="table table-striped table-bordered" style="width:100%">
    <thead>
        <tr>
            <th>Company</th>
            <th>Website</th>
            <th>Email</th>
            <th>Date Registered</th>
            <th>Total Employee</th>
            <th>View</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data['companies'] as $company)
        <tr>
            <td>{{$company->name}}</td>
            <td>{{$company->website}}</td>
            <td>{{$company->email}}</td>
            <td>{{$company->created_at}}</td>
            <td>{{$company->employees()->count()}}</td>
            <td>
                <form method="POST" action="company/{{$company->id}}">
                <a href="company/{{$company->id}}/edit" class="btn btn-warning">Update Profile</a>  
                <a href="company/{{$company->id}}" class="btn btn-primary">Detail</a>
                @method("delete")
                @csrf
                <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection

@section('js')

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready( function () {
        $('#example').DataTable();
    } );
</script>

@endsection