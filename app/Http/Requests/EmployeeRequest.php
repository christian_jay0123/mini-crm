<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(request()->isMethod('put'))
        {
            $validate =  [
                'email'=> 'required|email',
                'firstname'=>'required',
                'lastname'=>'required',
            ];
        }


        if(request()->isMethod('post'))
        {
            $validate = [
                'email'=> 'required|email',
                'firstname'=>'required',
                'lastname'=>'required',
                'password'=>'required',
            ];
        }

        return $validate;

    }
}
