<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Employee;
use App\Http\Requests\EmployeeRequest;
use Illuminate\Support\Facades\Hash;
class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($language,$company_id)
    {
        $data = ['language'=>$language, 'company_id' =>$company_id];
        return view('admin.employee.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request,$language,$company_id)
    {
        $request->validated();
        $employee = Employee::create([
            'firstname'=>$request->firstname,'lastname'=>$request->lastname,
            'email'=>$request->email,'password'=>Hash::make($request->password),
            'phone'=>$request->phone,'company_id' => $company_id,
        ]);
        return redirect("/$language/admin/company/$company_id/employee/$employee->id/edit");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($language,$company_id,Employee $employee)
    {
        $data = ['language'=>$language, 'company_id' =>$company_id, 'employee_info' => $employee];
        return view('admin.employee.edit',compact(['data']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, $language,$company_id, $id)
    {
        $validated = $request->validated();
        $employee = Employee::findOrFail($id);
        $password = $employee->password;
        if($request->password != null)
        {
            $password = $request->password;
        }

        $employee->update([
            'firstname'=>$request->firstname,'lastname'=>$request->lastname,
            'email'=>$request->email,'password'=>Hash::make($password),
            'phone'=>$request->phone
            ]);
        
            return redirect()->back();
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($language, $company_id, $id)
    {
    Employee::findorFail($id)->delete();
     return redirect()->back();   
    }
}
