<?php

namespace App\Http\Controllers\Admin\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Company;
use App\Mail\NewCompany;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\CompanyRequest;
use Image;
use File;
class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($language)
    {
        $data = [
            'language' => $language
        ];
        return view('admin.company.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request,$language)
    {

        $request->validated();
        $image_name = null;
        if($request->hasFile('logo')){
        $image = $request->file('logo');
        
        $image_name = time(). '.'.$image->getClientOriginalExtension();

        $destinationpath = public_path('/images/company/thumbnail');

        $resize = Image::make($image->getRealPath());

        $resize->resize(100,100,function($constraint){
            $constraint->aspectRatio();
        })->save($destinationpath.'/'. $image_name);

        $destinationpath = public_path('/images/company/logo');

        $image->move($destinationpath,$image_name);
}
        $company = Company::create([
            'logo'=>$image_name,'email'=>$request->email,
            'website'=>$request->website,'name'=>$request->name
        ]);

        Mail::to('test@ytest.com')->send(new NewCompany(['company'=>$company]));

        
        
        return redirect("$language/admin/company/$company->id");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($language, Company $company)
    {
        return view('admin.company.show',compact(['company']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($language,Company $company)
    {
        $data = [
            'language' => $language,'company'=>$company
        ];
        return view('admin.company.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $request,$language, $id)
    {
        $request->validated();
        $company = Company::findOrFail($id);
        $image_name = $company->logo;
        if($request->hasFile('logo'))
        {
        $thumbnail = public_path("/images/company/thumbnail/$image_name");
        $logo = public_path("/images/company/logo/$image_name");
        File::delete($logo,$thumbnail);


        $image = $request->file('logo');
        
        $image_name = time(). '.'.$image->getClientOriginalExtension();
        $destinationpath = public_path('/images/company/thumbnail');

        $resize = Image::make($image->getRealPath());

        $resize->resize(100,100,function($constraint){
            $constraint->aspectRatio();
        })->save($destinationpath.'/'. $image_name);

        $destinationpath = public_path('/images/company/logo');

        $image->move($destinationpath,$image_name);
        }


        $company->update([
            'logo'=>$image_name,'email'=>$request->email,
            'website'=>$request->website,'name'=>$request->name
        ]);

        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($language,$id)
    {
        $company = Company::findOrFail($id);
        $thumbnail = public_path("/images/company/thumbnail/$company->logo");
        $logo = public_path("/images/company/logo/$company->logo");
        File::delete($logo,$thumbnail);
        $company->delete();
        return redirect()->back();
    }
}
