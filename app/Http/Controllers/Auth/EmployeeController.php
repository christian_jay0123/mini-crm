<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Model\Employee;
use App\Http\Requests\EmployeeRequest;
class EmployeeController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('guest:employee');
    }

    public function showLoginForm()
    {
        return view('employee.login');
    }

    public function login(EmployeeRequest $request)
    {
       if(Auth::guard('employee')->attempt(['email'=>$request->email,'password'=>$request->password]))
       {
        return redirect()->intended(route('employee.index.index'));
       }

       return redirect()->back()->withInput($request->only('email','remember'));
    }
}
