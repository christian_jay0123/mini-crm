<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Employee;
class Company extends Model
{
    //
    protected $guarded = [];

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }
}
